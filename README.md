# Trabalho Prático 2

## Integrantes:
* Daniel Gonçalves
* Daniel Lopes
* Vitor Lemos

## Intruções
Para implantar o site basta instalar os pacotes:

`pip install -r requirements.txt`

Executar as migrações:

`python manage.py makemigrations`

`python manage.py migrate`

E executar o servidor WEB:

`python manage.py runserver`