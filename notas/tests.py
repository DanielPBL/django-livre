from django.test import TestCase, Client
from django.urls import reverse
from notas.models import Profile, Note, Reminder
from django.contrib.auth.models import User
import datetime

# Create your tests here.
class ProfileTestCase(TestCase):
    def setUp(self):
        user = User.objects.create_user('user', password='senha1234', first_name='User', last_name='Test')
        user.refresh_from_db()
        user.profile.birth_date = '1900-01-01'
        user.profile.job = 'Tester'
        user.profile.location = 'Neverland, Earth'
        user.save()
    
    def test_profile_association(self):
        user = User.objects.get(username='user')
        self.assertEqual(user.get_full_name(), 'User Test')
        self.assertEqual(user.profile.birth_date, datetime.date(1900, 1, 1))
        self.assertEqual(user.profile.job, 'Tester')
        self.assertEqual(user.profile.location, 'Neverland, Earth')

class NoteTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create_user('user', password='senha1234', first_name='User', last_name='Test')
        user2 = User.objects.create_user('stub', password='senha1234', first_name='Stub', last_name='User')
        Note.objects.create(title='Test Title', text='Test Text', user=user1)
        Note.objects.create(title='Test Title', text='Test Text', user=user2)
        self.client = Client()
        response = self.client.login(username='user',password='senha1234')
        self.assertEqual(response, True)
    
    def test_create(self):
        response = self.client.post(reverse('save'), {
            'title': 'Test Title',
            'text': 'Test Text',
            'type': 'Note'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['success'], True)
        self.assertEqual(response.json()['id'], 3)
    
    def test_read(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.context['notes']), 1)
        note = response.context['notes'][0]
        self.assertEqual(note.id, 1)
        self.assertEqual(note.title, 'Test Title')
        self.assertEqual(note.text, 'Test Text')
        self.assertEqual(note.status, 0)
    
    def test_update(self):
        response = self.client.post(reverse('save'), {
            'pk': 1,
            'title': 'Test Title Updated',
            'text': 'Test Text Updated',
            'type': 'Note'
        })
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['success'], True)
        self.assertEqual(response.json()['id'], 1)
        note = Note.objects.get(pk=1)
        self.assertEqual(note.title, 'Test Title Updated')
        self.assertEqual(note.text, 'Test Text Updated')
    
    def test_delete(self):
        note = Note.objects.all()
        self.assertEqual(len(note), 2)
        response = self.client.post(reverse('remove_note', kwargs={'pk': 1}), follow=True)
        self.assertEqual(response.status_code, 200)
        note = Note.objects.all()
        self.assertEqual(len(note), 1)
    
