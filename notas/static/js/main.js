const csrftoken = jQuery("[name=csrfmiddlewaretoken]").val();

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});

$('#text').focus(() => {
    $('input[name="title"]').removeClass('invisible');
});

$('#controls').mouseleave(() => {
    $('input[name="title"]').addClass('invisible');
});

$('#text').on('input', () => {
    $('input[name="title"]').removeClass('invisible');
});

$('#saveNote, #saveReminder').click((event) => {
    let title = $('#title').val();
    let text = $('#text').html();
    let data = {
        title: title,
        text: text,
        type: $(event.currentTarget).val()
    };

    $.ajax({
        url: SAVE_AJAX_URL,
        method: 'POST',
        data: data,
        dataType: 'json',
        success: (resp) => {
            if (resp.success) {
                //Nova nota
                if (resp.html) {
                    let $note = $(resp.html).addClass('w3-animate-right');
                    $note.find('form').append($("[name=csrfmiddlewaretoken]").first().clone());
                    $('#ctrs').after($note);
                    $('#title').val('');
                    $('#text').html('');
                }
                //Novo lembrete
                if (resp.menu) {
                    $('#container-reminders').html(resp.menu);
                    $('#title').val('');
                    $('#text').html('');
                }

                addEvents();
            }
        }
    });
});

$('#saveChange').click((event) => {
    let title = $('#title').val();
    let text = $('#text').html();
    let data = {
        title: title,
        text: text,
        type: 'Note',
        pk: $(event.currentTarget).val()
    };

    $.ajax({
        url: SAVE_AJAX_URL,
        method: 'POST',
        data: data,
        dataType: 'json',
        success: (resp) => {
            if (resp.success) {
                $('#note-' + resp.id).find('.note-title').html(data.title);
                $('#note-' + resp.id).find('.note-text').html(data.text);
                addEvents();
                $('#cancelChange').click();
            }
        }
    });
});

$('#cancelChange').click(() => {
    $('#title').val('');
    $('#text').html('');
    $('#saveChange, #cancelChange').addClass('invisible');
    $('#saveChange').val('');
    $('#saveNote, #saveReminder').removeClass('invisible');
});

function addEvents() {
    $('.remove').click((event) => {
        event.preventDefault();
        let link = $(event.currentTarget).attr('href');
        $('#remove').attr('action', link).submit();
    });

    $('.edit').click((event) => {
        let $button = $(event.currentTarget);
        let id = $button.val();
        let title = $button.parent().find('.note-title').html();
        let text = $button.parent().find('.note-text').html();
        $('#saveChange, #cancelChange').removeClass('invisible');
        $('#saveChange').val(id);
        $('#saveNote, #saveReminder').addClass('invisible');
        $('#title').val(title);
        $('#text').html(text).focus();
    });
}

addEvents();