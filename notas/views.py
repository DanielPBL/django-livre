from django.shortcuts import render
from django.views import View
from django.db.models import F,ExpressionWrapper,DecimalField, Sum
from django.http import HttpResponseRedirect
from django.forms import ModelForm
from django.urls import reverse, reverse_lazy
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.views.generic.list import ListView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import login, authenticate
from django.http import JsonResponse, HttpResponseForbidden
from django.template.loader import render_to_string
from django.views.decorators.cache import cache_page
from .models import Profile, Note, Reminder
from .forms import SignUpForm

class MainView(LoginRequiredMixin, ListView):
    template_name = 'index.html'
    context_object_name = 'notes'
    
    def get_queryset(self):
        return Note.objects.filter(user=self.request.user).order_by('-id')
    
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['reminders'] = Reminder.objects.filter(user=self.request.user)
        return context

def save(request):
    if request.user.is_authenticated:
        resp = dict()
        try:
            if request.POST['type'] == 'Note':
                if 'pk' in request.POST:
                    note = Note.objects.get(pk=request.POST['pk'])
                    note.title = request.POST['title']
                    note.text = request.POST['text']
                else:
                    note = Note(title=request.POST['title'], text=request.POST['text'], user=request.user)
                note.save()
                note.refresh_from_db()
                resp['success'] = True
                resp['id'] = note.id
                resp['html'] = render_to_string('note.html', {'note': note})
            elif request.POST['type'] == 'Reminder':
                reminder = Reminder(text=request.POST['text'], user=request.user)
                reminder.save()
                reminder.refresh_from_db()
                resp['success'] = True
                resp['id'] = reminder.id
                context = dict()
                context['reminders'] = Reminder.objects.filter(user=request.user)
                #context['user'] = request.user
                resp['menu'] = render_to_string('reminders.html', context, request)
            else:
                raise Exception('Unknown type!')
        except Exception as e:
            resp['error'] = repr(e)
        return JsonResponse(resp)
    else:
        raise Exception('teste')
        return HttpResponseForbidden()

@cache_page(60 * 15)
def show_signup(request):
    return render(request, 'signup.html', {'form': SignUpForm})

class SignUpView(View):
    def get(self, request):
        return show_signup(request)

    def post(self, request):
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.birth_date = form.cleaned_data.get('birth_date')
            user.profile.job = form.cleaned_data.get('job')
            user.profile.location = form.cleaned_data.get('location')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return HttpResponseRedirect(reverse('home'))
        else:
            return render(request, 'signup.html', {'form': form})


class EditView(LoginRequiredMixin, UpdateView):
    model = Profile
    fields = ['job', 'location', 'birth_date', 'img_user']
    template_name = "edit.html"
    success_url = reverse_lazy('home')

class DeleteNoteView(LoginRequiredMixin, DeleteView):
    model = Note
    success_url = reverse_lazy('home')

class DeleteReminderView(LoginRequiredMixin, DeleteView):
    model = Reminder
    success_url = reverse_lazy('home')
    