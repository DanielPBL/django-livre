from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User

class SignUpForm(UserCreationForm):
    job = forms.CharField(max_length=100)
    location = forms.CharField(max_length=100)
    birth_date = forms.DateField()

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'password1', 'password2', 'birth_date', 'job', 'location')